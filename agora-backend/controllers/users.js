var User = require("../models/User");
var Server = require("../models/Server");
var bcrypt = require("bcrypt");
var jwt = require('jsonwebtoken');
var fs = require('fs');
var path = require('path');
require("dotenv").config({ path: '/app/.env' });
var serversController = require('/app/agora-backend/controllers/servers');

exports.registerUser = async (req, res, next) => {
    try {
        // Generate a salt to make the database more secure for the passwords
        // Await the function to generate the salt
        var salt = await bcrypt.genSalt();
        // Hash the plain text password using the hash function and the salt generated
        var hashedPassword = await bcrypt.hash(req.body.password, salt);
        var user = {
            username: req.body.username,
            email: req.body.email,
            password: hashedPassword,
            birthDate: Date.now(),
        }
        // Create a new user using the mongoose model and the data stored previously
        var newUser = new User(user);
        // Await and save the user in the database
        var response = await newUser.save();
        console.log(response);
        // Return the response with status 200 meaning everything it's okay
        console.log(`User '${response._id}-${response.username}' registered succesfully`);
        return res.status(200).jsonp(response);

    } catch (errors) {
        // Print the error in the console
        console.log("Error adding the new user to the database: " + errors);
        errors = errors.errors;
        // Build an object to print errors in the view
        var errorsFormated = [];
        // Iterate the object and check which error is it
        for (error in errors) {
            // If the problem is something with unique constraints add custom message
            if (errors[error].kind == "unique") {
                // Push the object into the array
                errorsFormated.push({
                    error: "Duplicated field",
                    message: errors[error].path + " already in use"
                });
            } else {
                // Push the object into the array
                errorsFormated.push({
                    error: "Error with database",
                    message: "Something went wrong with the database"
                });
            }
        }
        // Return the response with status 400
        res.status(400).jsonp(errorsFormated);
    }
    next();
}

exports.loginUser = async (req, res, next) => {
    var message, status;
    try {
        // Get the user with the username that came from the request
        var user = await User.findOne({ username: req.body.username });
        // Check if the user does or doesn't exist
        if (user == null) {
            // Generate the error message and the status
            message = 'Username does not exist';
            status = 400;
            throw "The username does not exist in the database";
        }
        // Wait till bcrypt compares both passwords and returns a response
        if (await bcrypt.compare(req.body.password, user.password)) {
            status = 200;
            // Get the id of the user to store it in the token
            var user = { id: user.id };
            // Generate the token with the user id
            var token = await jwt.sign(user, process.env.JWT_SECRET, {
                // Make the token expire in 30 days
                expiresIn: "30d"
            });
        } else {
            // If password is incorrect we send a response to the client
            status = 400;
            message = 'Password is incorrect';
            throw "The password sent is incorrect";
        }
        // If there are no errors send the message correctly and add the token to the header
        console.log(`User '${user._id}-${user.username}' logged in succesfully`);
        return res.status(status).send(token);
    } catch (error) {
        // Print the error in the console
        console.log("Error login the user: " + error);
        if (status == 200) {
            status = 400;
            message = "Something went wrong"
        }
        // Build an object to print errors in the view 
        return res.status(status).send(message);
    }
    next();
}

exports.getUserInformation = async (req, res, next) => {
    try {
        // Get verified token
        var verified = req.verified;
        // Get the information of the user with the id inside the token
        var userInformation = await User.findOne({ _id: verified.id }).select(['-password', '-birthDate', '-__v']);
        console.log(userInformation);
        // Return the information back to the client
        console.log(`Info of user '${userInformation._id}-${userInformation.username}' gotten succesfully`);
        return res.status(200).jsonp(userInformation);
    } catch (error) {
        // Log the error
        console.log('Error getting user information: ' + error);
        // Send an error back to the client saying something went wrong
        res.status(400).send('Error getting user information');
    }
    next();
}

exports.updateInformation = async (req, res, next) => {
    try {
        // Get verified token
        var verified = req.verified;
        // Get id of the user via the token
        var id = verified.id;
        // Get user info
        var userInformation = await User.findOne({ _id: id });
        // Updated information of the user
        var update = {
            username: req.body.username,
            email: req.body.email,
        }
        console.log(update);
        // Interact with image if exists, do nothing otherwise
        if (req.files != undefined || req.files != null) {
            // Get extension of the filename
            var extension = path.extname(req.files.image.name);
            // Modify the filename
            req.files.image.name = `${id}${extension}`;
            update.image = req.files.image.name;
            // Remove file from folder
            var imagePath = '/app/agora-frontend/public/users/' + userInformation.image;
            // Delete file if exists
            if (fs.existsSync(imagePath)) {
                fs.unlinkSync(imagePath);
            }
            // Save image in public folder for frontend
            req.files.image.mv('/app/agora-frontend/public/users/' + req.files.image.name);
        }
        // Update informartion
        await User.updateOne({ _id: id }, update, { runValidators: true, context: 'query' });
        // Send status 200 and ok message
        console.log(`Info of user '${userInformation._id}-${userInformation.username}' updated succesfully`);
        return res.status(200).send('Information updated succesfully');
    } catch (error) {
        // Print error
        console.log('Error updating the information of the user: ' + error);
        if (error.errors != undefined || error.errors != null) {
            errors = error.errors;
            // Build an object to print errors in the view
            var errorsFormated = [];
            // Iterate the object and check which error is it
            for (error in errors) {
                // If the problem is something with unique constraints add custom message
                if (errors[error].kind == "unique") {
                    // Push the object into the array
                    errorsFormated.push({
                        error: "Duplicated field",
                        message: errors[error].path + " already in use"
                    });
                } else {
                    // Push the object into the array
                    errorsFormated.push({
                        error: "Error with database",
                        message: "Something went wrong with the database"
                    });
                }
            }
            // Return the response with status 400
            res.status(400).jsonp(errorsFormated);
        } else {
            // Return the response with status 400
            res.status(400).jsonp('Oops, there was an error updating user information');
        }
    }
    next();
}

exports.getUserListInfo = async (req, res, next) => {
    try {
        // Get verified token
        var verified = req.verified;
        // Get server id
        var serverId = req.params.serverId;
        // Check if the server exists
        var response = await serversController.doesServerExist(serverId);
        if (!response) {
            return res.status(400).send("The server does not exist");
        }
        // Get the server
        var server = await Server.findById(serverId);
        // Check if the user sending the request is a user or admin of the server
        var requestIsAdmin = await serversController.isUserAdmin(verified.id, serverId);
        var requestIsUser = await serversController.isUserUser(verified.id, serverId);
        if (!requestIsAdmin && !requestIsUser) {
            return res.status(400).send("You're not a user or admin of the server!");
        }
        // Create a query for user's and admin's info without the password and birthDate
        var users = await User.find({ '_id': { $in: server.users } }).select(['-password', '-birthDate', '-__v']);
        var admins = await User.find({ '_id': { $in: server.admins } }).select(['-password', '-birthDate', '-__v']);
        // Create an object with the arrays from queries
        var allUsers = {
            users: users,
            admins: admins
        }
        // Return ok
        console.log(`User '${verified.id}' got the info of the admins and users in server '${serverId}-${server.serverName}'`)
        return res.status(200).jsonp(allUsers);
    } catch (error) {
        console.log('Error getting user list information: ' + error);
        res.status(400).send('Error getting user list information!');
    }
    next()
}

exports.getServersFromUser = async (req, res, next) => {
    try {
        // Get verified token
        var verified = req.verified;
        // Get the servers that contain the user
        var servers = await Server.find({ $or: [{ users: verified.id }, { admins: verified.id }]}).select(['-channels', '-admins', '-users']);
        // Return ok
        console.log(`User ${verified.id} got the list of their servers`);
        return res.status(200).jsonp(servers);
    } catch (error) {
        console.log('Error getting all the servers from user: ' + error);
        res.status(400).send('Error getting the list of your servers');
    }
    next();
}