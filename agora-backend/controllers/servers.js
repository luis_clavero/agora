var mongoose = require("mongoose");
var Server = require("../models/Server");
var User = require("../models/User");
var path = require("path");
require("dotenv").config({ path: '/app/.env' });

// Function to check if the user is an admin in the server
exports.isUserAdmin = async (userId, serverId) => {
    // Get the server from the database
    var server = await Server.findById(serverId);
    // Return true if the user is an admin false otherwise
    if (server.admins.includes(userId)) {
        return true;
    } else {
        return false;
    }
}

// Function to check if the user is a user in the server
exports.isUserUser = async (userId, serverId) => {
    // Get the server from the database
    var server = await Server.findById(serverId);
    // Return true if the user is a user false otherwise
    if (server.users.includes(userId)) {
        return true;
    } else {
        return false;
    }
}

// Function to check if the server exists
exports.doesServerExist = async (serverId) => {
    // Check if serverId is valid or not
    if (mongoose.Types.ObjectId.isValid(serverId)) {
        // Get the server from the database
        var server = await Server.findOne({ _id: serverId });
        // Return true if the server exists false otherwise
        if (server != null && server != undefined) {
            return true;
        } else {
            false
        }
    } else {
        return false;
    }
}

exports.createServer = async (req, res, next) => {
    try {
        // Get verified token
        var verified = req.verified;
        // Create server object
        var server = {
            serverName: req.body.serverName,
            channels: [],
            admins: [verified.id],
            users: []
        }
        console.log("ServerName:" + req.body.serverName);
        // Create the new server with the model
        var newServer = new Server(server);
        // Get extension of the filename
        var extension = path.extname(req.files.image.name);
        // Image path
        var imagePath = `/app/agora-frontend/public/servers/${newServer._id}${extension}`;
        // Move file
        req.files.image.mv(imagePath);
        // Assign image to the newServer
        newServer.image = `${newServer._id}${extension}`;
        // Await the response of the save in the database
        var response = await newServer.save();
        // Return ok
        console.log(`User '${verified.id}' has created server '${newServer._id}-${newServer.serverName}' succesfully`);
        return res.status(200).jsonp(response);
    } catch (error) {
        // Print the error in the server console
        console.log("Error creating server: " + error);
        // Return error back to the client
        return res.status(400).send('Error occurred trying to create a new server');
    }
}

exports.joinServerAsUser = async (req, res, next) => {
    try {
        // Get verified token
        var verified = req.verified;
        // Get the id of the server via the request
        var serverId = req.params.serverId;
        // Check if the server exists
        var response = await this.doesServerExist(serverId);
        if (!response) {
            return res.status(400).send("The server does not exist");
        }
        // Check if the server contains the user as user or admin
        var requestIsAdmin = await this.isUserAdmin(verified.id, serverId);
        var requestIsUser = await this.isUserUser(verified.id, serverId);
        if (requestIsAdmin || requestIsUser) {
            return res.status(400).send("Trying to join a server you're already in!");
        }
        // Add the user to the server user list
        await Server.updateOne({ _id: serverId }, { $push: { users: verified.id } });
        // Return ok
        console.log(`User '${verified.id}' has joined server '${serverId}' as user`);
        return res.status(200).send('Server joined correctly!');
    } catch (error) {
        // Print the error in the server console
        console.log("Error joining server: " + error);
        // Return error back to the client
        return res.status(400).send('Error occurred trying to join server');
    }
}

exports.joinServerAsAdmin = async (req, res, next) => {
    /* 
    It is possible to add an admin to the server when the client 
    that made the request is and admin on it
    */
    try {
        // Get verified token
        var verified = req.verified;
        // Get the id of the server via the request
        var serverId = req.params.serverId;
        // Check if the server exists
        var response = await this.doesServerExist(serverId);
        if (!response) {
            return res.status(400).send('Server does not exist!');
        }
        // Get the id of the user to be added as admin
        var userId = req.body.userId;
        // Check if userId is valid
        if (!mongoose.Types.ObjectId.isValid(userId)) {
            return res.status(400).send('User trying to add as admin invalid');
        }
        // Check if the user exists
        var user = await User.find({ _id: userId});
        if (!user) {
            return res.status(400).send("The user you're trying to add to the server does not exist");
        }

        // Check if the user doing the request is an admin
        var requestIsAdmin = await this.isUserAdmin(verified.id, serverId);
        // Check if user to be added is admin or user
        var userIsAdmin = await this.isUserAdmin(userId, serverId);
        var userIsUser = await this.isUserUser(userId, serverId);
        // Check if the server doesn't contain the user of the request as admin
        if (!requestIsAdmin) {
            return res.status(400).send("Cannot add users to the server as admins if you're not an admin!");
        }
        // If the server contains the user as user remove it from the user list
        if (userIsUser) {
            await Server.updateOne({ _id: serverId }, { $pull: { users: userId } });
            // If the server does not contain the user to be added stop the request
        } else if (userIsAdmin) {
            return res.status(400).send("Cannot add an admin to the server if it's already an admin");
        } else {
            return res.status(400).send("Cannot make a user admin if it's not in the server!");
        }
        // Add the user as admin
        await Server.updateOne({ _id: serverId }, { $push: { admins: userId } });
        // Return ok
        console.log(`User ${verified._id} added '${userId}' to the server '${serverId}' as admin`);
        return res.status(200).send('User added as admin correctly');
    } catch (error) {
        console.log('Error trying to join the server as admin: ' + error)
        return res.status(400).send("Error adding user as admin"); 
    }
}

exports.getServerInfo = async (req, res, next) => {
    try {
        // Get verified token
        var verified = req.verified;
        // Get the id of the server via the request
        var serverId = req.params.serverId;
        // Check if the server exists
        var response = await this.doesServerExist(serverId);
        if (!response) {
            return res.status(400).send('Server does not exist!');
        }
        // Get the server from the database
        var server = await Server.findById(serverId);
        // Check if the server doesn't contain the user as user or admin
        var requestIsAdmin = await this.isUserAdmin(verified.id, serverId);
        var requestIsUser = await this.isUserUser(verified.id, serverId);
        if (!requestIsAdmin && !requestIsUser) {
            return res.status(400).send("Cannot get the information of a server you're not in!");
        }
        // Mount in an object the server data and send it to the client
        var serverData = {
            id: server.id,
            serverName: server.serverName,
            channels: server.channels,
            image: server.image
        }
        // Return ok
        console.log(`Info of server '${serverData.id}-${serverData.serverName}' gotten succesfully`);
        return res.status(200).jsonp(serverData);
    } catch (error) {
        // Print error if occurs
        console.log('Error getting server data: ' + error);
        return res.status(400).send('Error getting server info!');
    }
}

exports.getAllServers = async (req, res, next) => {
    try {
        // Get verified token
        var verified = req.verified;
        // Get all servers that don't have the user doing the request without users, admins and channels 
        var servers = await Server.find({ $and : [{ admins : { $nin : [verified.id] }}, 
            { users : { $nin : [verified.id] }}]}).select(['-channels', '-admins', '-users']);
        // Return object to response
        return res.status(200).jsonp(servers);
    } catch (error) {
        // Print the error in the server console
        console.log("Error getting servers: " + error);
        // Return error back to the client
        return res.status(400).send("Error occurred trying to get all servers");
    }
}

exports.createChannel = async (req, res, next) => {
    try {
        // Get verified token
        var verified = req.verified;
        // Get the id of the server via the request
        var serverId = req.params.serverId;
        // Check if the server exists
        var response = await this.doesServerExist(serverId);
        if (!response) {
            return res.status(400).send("The server does not exist");
        }
        // Check if the server doesn't contain the user as user or admin
        var requestIsAdmin = await this.isUserAdmin(verified.id, serverId);
        if (!requestIsAdmin) {
            return res.status(400).send("Cannot create channels if you're not an admin of the server!");
        }
        // Build the channel object
        var channel = {
            _id: mongoose.Types.ObjectId(),
            channelType: req.body.channelType,
            name: req.body.name
        }
        // If the type is not chat nor voice channel return error
        if (channel.channelType != 'chat' && channel.channelType != 'voice') {
            return res.status(400).send("Channel type must be voice or chat!");
        }
        // Add the channel to the array of channels in the server
        await Server.updateOne({ _id: serverId }, { $push: { channels: channel } });
        // Return ok
        console.log(`Channel '${channel._id}' at server '${serverId}' created succesfully`);
        return res.status(200).send('Channel created succesfully!');
    } catch (error) {
        // Print error if occurs
        console.log('Error creating the channel in the server: ' + error);
        return res.status(400).send("Error creating channel!");
    }
}

exports.deleteChannel = async (req, res, next) => {
    try {
        // Get verified token
        var verified = req.verified;
        // Get the id of the server via the request
        var serverId = req.params.serverId;
        // Check if the server exists
        var response = await this.doesServerExist(serverId);
        if (!response) {
            return res.status(400).send("The server does not exist");
        }
        // Check if the server doesn't contain the user as user or admin
        var requestIsAdmin = await this.isUserAdmin(verified.id, serverId);
        if (!requestIsAdmin) {
            return res.status(400).send("Cannot delete channels if you're not an admin of the server!");
        }
        // Get the id of the channel
        var channelId = req.body.channelId;
        // Remove the channel to the array of channels in the server
        await Server.updateOne({ _id: serverId }, { $pull: { channels: { _id: channelId } } });
        // Return ok 
        console.log(`Channel ${channelId} at server '${serverId}' deleted succesfully`);
        return res.status(200).send('Channel deleted succesfully!');
    } catch (error) {
        // Print error if occurs
        console.log('Error deleting the channel in the server: ' + error);
        return res.status(400).send("Error deleting channel!");
    }
}
