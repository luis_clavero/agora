var mongoose = require("mongoose");
var Server = require("../models/Server");
var Message = require("../models/Message");
var serversController = require('/app/agora-backend/controllers/servers');
require("dotenv").config({ path: '/app/.env' });

exports.doesChannelExist = async (channelId, serverId) => {
    // Verify channelId is valid
    if (mongoose.Types.ObjectId.isValid(channelId)) {
        // Get server info
        var server = await Server.findOne({ _id : serverId, "channels._id": channelId });
        // Check if server has the channel
        if (server) {
            return true;
        } else {
            return false;
        }
    } else {
        return false
    }
}

exports.getMessagesFromServer = async (req, res, next) => {
    try {
        // Get verified token
        var verified = req.verified;
        // Get server id
        var serverId = req.params.serverId;
        // Check if the server exists
        var response = await serversController.doesServerExist(serverId);
        if (!response) {
            return res.status(400).send("The server does not exist");
        }
        // Check if the user sending the request is a user or admin of the server
        var requestIsAdmin = await serversController.isUserAdmin(verified.id, serverId);
        var requestIsUser = await serversController.isUserUser(verified.id, serverId);
        if (!requestIsAdmin && !requestIsUser) {
            return res.status(400).send("You're not a user or admin of the server!");
        }
        // Get messages from server
        var messages = await Message.find({ serverId: serverId });
        // Return ok and messages
        console.log(`User '${verified.id}' got all the messages in the server '${serverId}' succesfully`);
        return res.status(200).jsonp(messages);
    } catch (error) {
        // Print and return error
        console.log('Error getting messages from server: ' + error);
        res.status(400).send('Error getting messages from server!');
    }
    next()
}

exports.createNewMessage = async (req, res, next) => {
    try {
        // Get verified token
        var verified = req.verified;
        // Get server id
        var serverId = req.body.serverId;
        // Get channel id
        var channelId = req.body.channelId;
        // Check if the server exists
        var serverExists = await serversController.doesServerExist(serverId);
        if (!serverExists) {
            return res.status(400).send("The server does not exist");
        }
        // Check if channel exists
        var channelExists = await this.doesChannelExist(channelId, serverId);
        if (!channelExists) {
            return res.status(400).send("The channel you are trying to send a message does not exist");
        }
        // Check if the user sending the request is a user or admin of the server
        var requestIsAdmin = await serversController.isUserAdmin(verified.id, serverId);
        var requestIsUser = await serversController.isUserUser(verified.id, serverId);
        if (!requestIsAdmin && !requestIsUser) {
            return res.status(400).send("You're not a user or admin of the server!");
        }
        // Build the object message
        var message = {
            text: req.body.text,
            author: verified.id,
            channelId: channelId,
            serverId: serverId
        }
        // Construct the new message
        var newMessage = new Message(message);
        // Save it into the database
        await newMessage.save();
        // Return ok
        console.log(`User '${verified.id}' saved a message in the channel '${channelId}' in the server '${serverId}'`);
        return res.status(200).send('Message saved into the database');
    } catch (error) {
        // Print and return error
        console.log('Error saving message: ' + error);
        res.status(400).send('Error saving message into the database');
    }
    next()
}