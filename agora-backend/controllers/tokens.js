var Token = require("../models/Token");
require("dotenv").config({ path: '/app/.env' });

exports.blacklistToken = async (req, res, next) => {
    try {
        // Get the token from the header
        var token = {tokenId: req.header('authorization')};
        // Check if the token is present or not in the request
        if (!token) {
            res.status(400).send('Acces denied, token is required');
        }
        // Create the new blacklisted token
        var newBlacklistedToken = new Token(token);
        // Save the token in the database
        await newBlacklistedToken.save();
        return res.status(200).send('Token blacklisted correctly');
    } catch (error) {
        // If any errors occur print the error
        console.log("Error blacklisting the token: " + error);
        res.status(400).send('Error blacklisting the token');
    }
    next();
}
