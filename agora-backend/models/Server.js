var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Server schema in database for mongoose
var ServerSchema = new Schema({
    serverName: { type: String, required: true },
    createdAt: { type: Date, default: Date.now },
    channels: [{
        _id: { type: Schema.Types.ObjectId, required: true },
        channelType: { type: String, required: true },
        name: { type: String, required: true }
    }],
    admins: { type: [String] },
    users: { type: [String] },
    image: { type: String, default: "none.jpg" },
});

module.exports = mongoose.model('Server', ServerSchema);