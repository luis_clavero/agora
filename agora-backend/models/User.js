var mongoose = require('mongoose');
var Schema = mongoose.Schema;
// Mongoose unique validator
var uniqueValidator = require('mongoose-unique-validator');

// User schema in database for mongoose
var UserSchema = new Schema({
    username: { type: String, required: true, unique: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    birthDate: { type: Date, required: true },
    image: { type: String, default: "none.jpg" }
});

// Apply the validator
UserSchema.plugin(uniqueValidator);

module.exports = mongoose.model('User', UserSchema);