var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Message schema in database for mongoose
var MessageSchema = new Schema({
    text: { type: String, required: true },
    createdAt: { type: Date, default: Date.now },
    author: { type: String, required: true },
    channelId: { type: String, required: true },
    serverId: { type: String, required: true }
});

module.exports = mongoose.model('Message', MessageSchema);