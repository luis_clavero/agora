var mongoose = require('mongoose');
var Schema = mongoose.Schema;
// Mongoose unique validator
var uniqueValidator = require('mongoose-unique-validator');

// Token Blacklist schema in database for mongoose
var TokenSchema = new Schema({
    tokenId: { type: String, required: true, unique: true }
});

// Apply the validator
TokenSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Token', TokenSchema);