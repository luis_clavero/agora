// Required variables
var path = require("path");
var controllerDir = "/app/agora-backend/controllers";
var express = require("express");
var router = express.Router();

// Controllers
var usersController = require(path.join(controllerDir, "users"));
var tokensController = require(path.join(controllerDir, "tokens"));
var serversController = require(path.join(controllerDir, "servers"));
var messagesController = require(path.join(controllerDir, "messages"));

// USER ROUTES
// Call the controller for register if the api url is to register a new user
router.post("/user/register", usersController.registerUser);

// Call the controller to log in the user if the url is to login a user
router.post("/user/login", usersController.loginUser);

// Call the controller to get the information of the user with the token sent via the request
router.get("/user/info", usersController.getUserInformation);

// Update information of the user when the url is called
router.post("/user/updateInfo", usersController.updateInformation);

// Get the info of all users and admins of a server
router.get("/user/info/:serverId", usersController.getUserListInfo);

// Get the list of the servers of the user
router.get("/user/servers", usersController.getServersFromUser);



// TOKEN ROUTES
// Blacklist the token each time the url is called
router.post("/token/blacklist", tokensController.blacklistToken);



// SERVER ROUTES
// Create a new server 
router.post("/server/create", serversController.createServer);

// Join the server with the id of the server you want to join
router.get("/server/join/:serverId", serversController.joinServerAsUser);

// Add to the server a user as admin
router.post("/server/addAdmin/:serverId", serversController.joinServerAsAdmin);

// Get the information of the server related to the view in the client
router.get("/server/info/:serverId", serversController.getServerInfo);

// Get all servers except the ones the user is already in
router.get("/server/servers", serversController.getAllServers);

// Create a channel in the server
router.post("/server/createChannel/:serverId", serversController.createChannel);

// Delete a channel in the server
router.post("/server/deleteChannel/:serverId", serversController.deleteChannel);



// MESSAGE ROUTES
// Get all the messages from a server
router.get("/messages/server/:serverId", messagesController.getMessagesFromServer);

// Send a new message to a channel and server
router.post("/messages/new", messagesController.createNewMessage);

module.exports = router;