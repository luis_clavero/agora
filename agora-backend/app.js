// .env variables
require("dotenv").config({ path: '/app/.env' });
// Packages required 
var port = process.env.BACKEND_PORT;
var express = require('express');
var app = express();
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var cors = require('cors');
var fs = require('fs');
var jwt = require('jsonwebtoken');
var fileUpload = require('express-fileupload');
var Token = require("./models/Token");

// Start server
var server = require("https").createServer({
    // SSL and TLS certificates and keys to make nodejs go through https instead of http
    key: fs.readFileSync('/app/ssl/key.pem'),
    cert: fs.readFileSync('/app/ssl/cert.pem')
}, app).listen(port, () => {
    console.log("Server woking on port: " + port);
});

// Socket configuration
const io = require("socket.io")(server, {
  cors: {
    origin: "*",
  },
});
io.on('connection', (socket) => {
    // When a socket connectio is made print a log
    console.log(`Socket ${socket} connected`);

    // Join the text channel when the client clicks on it
    socket.on("joinedTextChannel", (textChannel) => {
      socket.leaveAll();
      socket.join(textChannel);
      console.log(socket.rooms);
    });

    // Send a message to the room when the 'newMsg' event occurs
    socket.on('newMsg', (message) => {
        console.log(message);
        socket.to(message.serverId + '@' + message.channelId).emit('newMsg', message);
    });
})

// Mongo database connection
mongoose.connect(
    `mongodb://${process.env.MONGO_ROOT_USER}:${process.env.MONGO_ROOT_PASSWORD}@${process.env.MONGO_URI}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}?authSource=admin`,
    { useCreateIndex: true, useUnifiedTopology: true, useNewUrlParser: true },
    (err, res) => {
        // If any errors occur connecting to database print error, print connection done otherwise
        if (err) console.log(`ERROR: connecting to Database. ${err}`);
        else console.log(`Database Online: ${process.env.MONGO_DB}`);
    }
);

// Configure the app to use bodyParser() to access request attributes
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

// Enable uploading files and accessing them via the request
// enable files upload
app.use(fileUpload());

// Setup cors to allow anyone to make calls to the backend server 
app.use(cors());

// Use a middleware for each route except login and register
app.use('/', async (req, res, next) => {
    // If route is /api/user/login or /apì/user/register use next()
    if (req.originalUrl == '/api/user/login' || req.originalUrl == '/api/user/register') {
        next()
    } else {
        try  {
            // Get the token
            var token = req.header('authorization');
            // Check if the token is in the blacklist
            var blacklistedToken = await Token.findOne({ tokenId: token });
            if (blacklistedToken) {
                return res.status(401).send('Access denied, token is blacklisted');
            }
            // Verify it's valid
            var verified = await jwt.verify(token, process.env.JWT_SECRET);
            // Add the verified token as an attribute of the request
            req.verified = verified;
            next();
        } catch (error) {
            // Print errors if occur
            console.log('Error validating token:' + error);
            return res.status(401).send('Error validating token');
        }
    }
});

// API routes file
var apiRoutes = require('./api-routes/index.js');

// Use the api routes file whenever an api url is called
app.use('/api', apiRoutes);

// Middleware to return error if the url used is not defined
app.use(function (req, res, next) {
    res.status(404).send('This URL cannot be found! (watch out with request methods)');
});


