import { createRouter, createWebHistory } from 'vue-router'
import LogIn from "../views/LogIn.vue";
import SignUp from '../views/SignUp.vue'
import Main from '../views/Main.vue'
import NewServer from '../views/NewServer.vue'
import UpdateUserInfo from '../views/UpdateUserInfo.vue'
//import axios from 'axios'
const routes = [
  {
    path: "/signup",
    name: "SignUp",
    meta: {
      authenticated: true,
    },
    component: SignUp,
  },

  {
    path: "/login",
    name: "LogIn",
    meta: {
      authenticated: true,
    },
    component: LogIn,
  },
  {
    path: "/update",
    name: "UpdateUserInfo",
    meta: {
      requiresAuth: true,
     
    },
    component: UpdateUserInfo,
  },

  {
    path: "/",
    name: "Main",
    meta: {
      //hasServers: true,
      requiresAuth: true
      
      
      
    },
    component: Main,
    

    
  },

  {
    path: "/servers",
    name: "NewServer",
    meta: {
      requiresAuth: true,

    },
    component: NewServer,
  },


];

const router = createRouter({
  history: createWebHistory(),
  routes
});




router.beforeEach((to, from, next)  => {

  /*let token = localStorage.getItem("jwt");
    var servers = []
      axios
      .get("https://localhost:4000/api/user/servers", {
        headers: { Authorization: token },
      })
      .then((response) => {
        servers = response.data;
        console.log(servers)
        
        
        
      });
    

  

  
  if (to.matched.some((record) => record.meta.hasServers)) {
    

    if (servers.length == 0) {
      
      router.push({ name: 'Main' });
      
      return;
      } else {
        
        next({
          path: "/",
        });
        
      }
    
  }  else {
        
    next();
    
  }*/


  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (localStorage.getItem("jwt") == null) {
      next({
        path: "/login",
      });
      
    } else {
      next();
    }
  } else {
        
    next();
    
  }

    
  
 

    if (to.matched.some((record) => record.meta.authenticated)) {
    if (localStorage.getItem("jwt") !== null) {
      next({
        path: "/",
      });
    } else {
      next();
    }
  } else {
    next();
  }



});



export default router
