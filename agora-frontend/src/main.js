import Vue from 'vue'
import { createApp } from 'vue'
import App from './App.vue'
import router from "./router";
import VueSession from 'vue-session'





createApp(App).use(router).mount('#app')
Vue.use(VueSession);
