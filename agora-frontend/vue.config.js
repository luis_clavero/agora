const fs = require('fs');

module.exports = {
    devServer: {
        https: {
          key: fs.readFileSync('/app/ssl/key.pem'),
          cert: fs.readFileSync('/app/ssl/cert.pem'),
        },
        public: 'https://localhost:3000/'
    }
}